<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Address;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Redirect;
use DataTables;
class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'yajraDT']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    //get data using yajra
    public function yajraDT()
    {
        /*return Datatables::of(User::query())
        ->addColumn('delete', "{!! Form::open(['action' => ['UsersController@destroy',". "7], 'method' => 'POST', 'class' => 'pull-right']) !!}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
        {!! Form::close()!!}")
        ->rawColumns(['delete'])
        ->make(true);*/

        /*$users = User::all();
        return Datatables::of($users)
        ->addColumn('delete', function ($users) {
                return '{!! Form::open(["action" => ["UsersController@destroy",'.$users->id.'], "method" => "POST", "class" => "pull-right"]) !!}
                {{ Form::hidden("_method", "DELETE") }}
                {{ Form::submit("Delete", ["class" => "btn btn-danger"]) }}
                {!! Form::close()!!}")';
        })
        ->rawColumns(['delete'])
        ->make(true);*/
        $users = DB::table('users')
        ->join('addresses', 'users.id', '=', 'addresses.user_id')
        ->select('users.*', 'addresses.city', 'addresses.barangay')
        ->get();

         return Datatables::of($users)
        ->addColumn('delete', function ( $users ) {
            return view('users.delete', compact('users'))->render();
        })
        ->addColumn('edit', function ( $users ) {
            return view('users.editmodal', compact('users'))->render();
        })
        ->escapeColumns([])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('users.create');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'email' => 'required',
            'password' => 'required'
        ]);

        $user = new User;
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->save();
        
        return redirect('/users')->with('success', 'User Created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $userAddress = Address::where('user_id', $id)->first();

        return view('users.show', [

            'user' => $user,
            'userAddress' => $userAddress
        ]);
    }

    /*public function showProfile($id)
    {
        $user = User::find($id);

        return view('users.profile', [

            'user' => $user
        ]);
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $address = Address::where('user_id', $id)->first();

        return view('users.edit', [

            'user' => $user,
            'address' => $address
        ]);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'first_name' => 'required',
            'last_name' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'profile_picture' => 'image|nullable|max:1999'
        ]);

        //handle file upload. If user actually uploaded a file, upload. Else, no image.
        if($request->hasFile('profile_picture'))
        {
            //purpose of this is so if someone uploads same name.
            //get file name
            $filenameWithExt = $request->file('profile_picture')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('profile_picture')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('profile_picture')->storeAs('public/profile_pictures', $fileNameToStore);
        }
        else
        {
            $fileNameToStore = 'noimage.jpg';
        }

        $user = User::find($id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->profile_picture = $fileNameToStore;

        if($user->save())
        {
            $address = Address::where('user_id', $id)->first();
            $address->city = $request->input('city');
            $address->barangay = $request->input('barangay');
        }

        $address->save();

        return redirect('/users')->with('success', 'User Updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/users')->with('success', 'User Deleted.');
    }
}
