<?php

namespace App\Http\Controllers;

use App\User;
use App\Address;
use Illuminate\Http\Request;
use Datatables;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['services', 'index']]);
    }


    public function index()
    {
        $title = 'Welcome To Laravel/Index page';

    	return view('pages.index', [
            'title' => $title
        ]);
    }

    /*public function about()
    {
        $title = 'This is about page';

    	return view('pages.about', [

            'title' => $title
        ]);
    }*/

    public function services()
    {
        $title = 'This is services page';
        $services_array = ['Web Dev', 'Software Dev', 'Machine Learning'];

    	return view('pages.services', [

            'title' => $title,
            'services_array' => $services_array
        ]);
    }

    public function showProfile($id)
    {
        $user = User::find($id);
        $userAddress = Address::where('user_id', $id)->first();

        return view('pages.profile', [

            'user' => $user,
            'userAddress' => $userAddress
        ]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $address = Address::where('user_id', $id)->first();

        return view('pages.profile_edit', [

            'user' => $user,
            'address' => $address
        ]);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'first_name' => 'required',
            'last_name' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'profile_picture' => 'image|nullable|max:1999'
        ]);

        //handle file upload. If user actually uploaded a file, upload. Else, no image.
        if($request->hasFile('profile_picture'))
        {
            //purpose of this is so if someone uploads same name.
            //get file name
            $filenameWithExt = $request->file('profile_picture')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('profile_picture')->getClientOriginalExtension();
            //filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //upload image
            $path = $request->file('profile_picture')->storeAs('public/profile_pictures', $fileNameToStore);
        }
        else
        {
            $fileNameToStore = 'noimage.jpg';
        }

        $user = User::find($id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->profile_picture = $fileNameToStore;

        if($user->save())
        {
            $address = Address::where('user_id', $id)->first();
            $address->city = $request->input('city');
            $address->barangay = $request->input('barangay');
        }

        $address->save();

        return redirect('/profile/'.$id)->with('success', 'User Updated.');
    }
}