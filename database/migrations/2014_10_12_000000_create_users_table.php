<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->text('description')->nullable();
            $table->string('gender', 1)->nullable();
            $table->text('profile_picture')->nullable();
            $table->string('email')->unique();
            $table->string('type');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('users')->insert(
            array(
                'id' => 28,
                'first_name' => 'Karl Anthony',
                'last_name' => 'Arnejo',
                'description' => 'This is a description.',
                'gender' => 'M',
                'profile_picture' => 'noimage.jpg',
                'email' => 'karlanthonyarnejo@gmail.com',
                'password' => '$2y$10$r3PXWlH1amwLwakbbqkqOuen7MT.csC93hH2/UF/2gITwr89GcRh6',
                'remember_token' => 'M3FJH4WvCAbmqKxCQqn5TdtQArQ2NOPz7wQtDeyhZf8qOZTiorJPAIg72qjz',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Admin'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 29,
                'first_name' => 'Emily',
                'last_name' => 'Blunt',
                'description' => 'This is a description.',
                'gender' => 'F',
                'profile_picture' => 'noimage.jpg',
                'email' => 'eb@gmail.com',
                'password' => '$2y$10$Y7fsf3MscqBVg676/C8Q4OD2oRdYdsYjGPxzK/74TdUwL/aYgYtg2',
                'remember_token' => 'pmsQd5lzsewVmkVMaf1YoBz9KXGzcb7A9KFr30JbXQKlzLtdGeNFpcttBY4n',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 30,
                'first_name' => 'John',
                'last_name' => 'Doe',
                'description' => 'This is a description.',
                'gender' => 'M',
                'profile_picture' => 'noimage.jpg',
                'email' => 'john@gmail.com',
                'password' => '$2y$10$EaAp3ARcWvfd/WA6962A7uJpJNcwMq.VdFZzsLuhzfFRbaZrCehI.',
                'remember_token' => 'jCkEbNF8l5VyWiVCFb7rQXIYlJgGzSs0ON4Nl1tYEkZsUl38blmlPtV2FPpN',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 31,
                'first_name' => 'Daisy',
                'last_name' => 'Sanchez',
                'description' => 'This is a description.',
                'gender' => 'F',
                'profile_picture' => 'noimage.jpg',
                'email' => 'ds@gmail.com',
                'password' => '$2y$10$yiDbC/6eFbz26CNZ.dvHFO521x06UpiJXRFoCX2gz9jalbnPNEpiS',
                'remember_token' => 'JaVe7WfeO5j9CYktGQvUJIXHp8HnYGa2hpUqxDbBgf6qb5t3OHoaNNhjbOfw',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 32,
                'first_name' => 'Jan Benedict',
                'last_name' => 'David',
                'description' => 'This is a description.',
                'gender' => 'M',
                'profile_picture' => 'noimage.jpg',
                'email' => 'jb@gmail.com',
                'password' => '$2y$10$E0VNjMac8jSgL2wIcRT3Y.EJJ7Ji8aFxdR3KxiLSBBbrO5xa6vzFi',
                'remember_token' => 'cefzkJneVd8SSevf50xipXPnylO2OIr6r1hE6JHHsNxOlns3an9l2Gc3Tkjf',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 34,
                'first_name' => 'Johnny',
                'last_name' => 'Depp',
                'description' => 'This is a description.',
                'gender' => 'M',
                'profile_picture' => 'noimage.jpg',
                'email' => 'jd@gmail.com',
                'password' => '$2y$10$i97wNYOBLQf19p.jUYwgdeXFhWOfwwkEyadY9QJI.FCYNISkU8FZm',
                'remember_token' => 'PO6kJ0avedEI8FflzWSnPVYXcVUTYbePaokfrmm8sg8BNkFrbCvCTAO4Ko0U',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 35,
                'first_name' => 'Karen',
                'last_name' => 'Pagapulaan',
                'description' => 'This is a description.',
                'gender' => 'F',
                'profile_picture' => 'noimage.jpg',
                'email' => 'kp@gmail.com',
                'password' => '$2y$10$xkX6BPtIzYZieDSlOjhM6ePCdIk9jeFZmwJw7Wc9pnW.Ly2K/tVJe',
                'remember_token' => 'xKbW6haUZarZl7SqPtMD98SsTs0XYx1UCA9dXTLcGJNPeCszM8qFYcjWlE8G',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 36,
                'first_name' => 'Alexis',
                'last_name' => 'Castro',
                'description' => 'This is a description.',
                'gender' => 'F',
                'profile_picture' => 'noimage.jpg',
                'email' => 'ac@gmail.com',
                'password' => '$2y$10$1TKbrQL9Wdzh3qxYcJpXQOKCE/B4yEw53qIrkA358nAcL7UAQY7o.',
                'remember_token' => 'RHIrBFlKJ6taaO4UbvV1SQmzplZfnTAnpGdkfLdadyi2qgeYBV5ZrJuMFGe2',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
        DB::table('users')->insert(
            array(
                'id' => 49,
                'first_name' => 'Justine Shane',
                'last_name' => 'Macarat',
                'description' => 'This is a description.',
                'gender' => 'F',
                'profile_picture' => 'noimage.jpg',
                'email' => 'jm@gmail.com',
                'password' => '$2y$10$aLwudGPhbnpq3Bfh5C3Mju2C4Av4Sg6olUle2Gld3SlCIBIC3TbpG',
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28',
                'type' => 'Regular'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
