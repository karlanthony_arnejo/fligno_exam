<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('city')->nullable();
            $table->string('barangay')->nullable();
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });

        // Insert some stuff
        DB::table('addresses')->insert(
            array(
                'id' => 13,
                'user_id' => 28,
                'city' => 'Iligan',
                'barangay' => 'Suarez',
                'lat' => 8.1964,
                'long' => 124.214,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 14,
                'user_id' => 29,
                'city' => 'Cagayan De Oro',
                'barangay' => 'Carmen',
                'lat' => 8.4676,
                'long' => 124.6221,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 15,
                'user_id' => 30,
                'city' => 'Iligan',
                'barangay' => 'Buru-un',
                'lat' => 8.1825,
                'long' => 124.1782,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 16,
                'user_id' => 31,
                'city' => 'Iligan',
                'barangay' => 'Tubod',
                'lat' => 8.2159,
                'long' => 124.2388,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 17,
                'user_id' => 32,
                'city' => 'Iligan',
                'barangay' => 'Tambacan',
                'lat' => 8.2224,
                'long' => 124.2345,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 19,
                'user_id' => 34,
                'city' => 'Cagayan De Oro',
                'barangay' => 'Bugo',
                'lat' => 8.508,
                'long' => 124.759,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 20,
                'user_id' => 35,
                'city' => 'Iligan',
                'barangay' => 'Luinab',
                'lat' => 8.2433,
                'long' => 124.2727,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 21,
                'user_id' => 36,
                'city' => 'Iligan',
                'barangay' => 'Linamon',
                'lat' => 8.168,
                'long' => 124.1588,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
        DB::table('addresses')->insert(
            array(
                'id' => 22,
                'user_id' => 49,
                'city' => 'Pagadian',
                'barangay' => 'Alta Tierra',
                'lat' => 7.8249717,
                'long' => 123.4365816,
                'created_at' => '2019-02-16 18:05:06',
                'updated_at' => '2019-02-16 18:05:28'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
