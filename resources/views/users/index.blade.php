@extends('layouts.app')

@section('content')
	<h1>Users</h1>

<div class="container">
        <table class="table table-bordered" id="tableDT">
            <thead>
                <tr>
                    <th class="text-left">Id</th>
                    <th class="text-left">First Name</th>
                    <th class="text-left">Last Name</th>
                    <th class="text-left">Email</th>
                    <th class="text-left">Gender</th>
                    @if(Auth::check() && Auth::user()->type == "Admin")
                    <th class="text-left">Actions</th>
                    <th class="text-left">edit</th>

                    @endif
                </tr>
            </thead>
        </table>
</div>

<div id="editAluno" class="modal fade" role="dialog">
  <div class="modal-dialog">
  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar aluno</h4>
      </div>
      <div class="modal-body">
        <form role="form">
          <div class="form-group">
            <label>ID do Cartão</label>
            <input type="text" class="form-control" id="id_cartao" placeholder="ID do Cartão" value=>
          </div>
          <div class="form-group">
            <label>Nome do aluno</label>
            <input type="text" class="form-control" id="nome_aluno" placeholder="Nome do Aluno"/>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" id="email" placeholder="E-mail"/>
          </div>
          <div class="form-group">
            <label>Triénio</label>
            <input type="text" class="form-control" id="trienio" placeholder="Triénio"/>
          </div>
      </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-primary">Editar aluno</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </div>
    </div>
  </div>
</div>
@endsection