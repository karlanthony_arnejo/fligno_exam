<button class="btn btn-primary tag_cnt" style="top:144px; left:510px" onclick="showModal('data')" type="button" value="1">Edit</button>

<div class="container">
  <!-- Button to Open the Modal -->

  <!-- The Modal -->
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            {!! Form::open(['action' => ['UsersController@update', $users->id], 'method' => 'POST']) !!}
          <div class="form-group">
              {{ Form::label('first_name', 'First Name') }}
              {{ Form::text('first_name', $users->first_name, ['class' => 'form-control', 'placeholder' => 'First Name']) }}
          </div>
          <div class="form-group">
              {{ Form::label('last_name', 'Last Name') }}
              {{ Form::text('last_name', $users->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name']) }}
          </div>
          <div class="form-group">
              {{ Form::label('city', 'City') }}
              {{ Form::text('city', $users->city, ['class' => 'form-control', 'placeholder' => 'City']) }}
          </div>
          <div class="form-group">
              {{ Form::label('barangay', 'Barangay') }}
              {{ Form::text('barangay', $users->barangay, ['class' => 'form-control', 'placeholder' => 'Barangay']) }}
          </div>
              {{ Form::hidden('_method', 'PUT') }}
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
                {{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
            {!! Form::close() !!}
        </div>
        
      </div>
    </div>
  </div>
  
</div>

<script type="text/javascript">
  function showModal(data)
{
   //you can do anything with data, or pass more data to this function. i set this data to modal header for example
   $("#myModal .modal-title").html(data)
   $("#myModal").modal();
}

</script>