@extends('layouts.app')

@section('content')
	<h1>Edit User Profile</h1>
	{!! Form::open(['action' => ['UsersController@update', $user->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    	<div class="form-group">
    		{{ Form::label('first_name', 'First Name') }}
    		{{ Form::text('first_name', $user->first_name, ['class' => 'form-control', 'placeholder' => 'First Name']) }}
    	</div>
    	<div class="form-group">
    		{{ Form::label('last_name', 'Last Name') }}
    		{{ Form::text('last_name', $user->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name']) }}
    	</div>
        <div class="form-group">
            {{ Form::label('city', 'City') }}
            {{ Form::text('city', $address->city, ['class' => 'form-control', 'placeholder' => 'City']) }}
        </div>
        <div class="form-group">
            {{ Form::label('barangay', 'Barangay') }}
            {{ Form::text('barangay', $address->barangay, ['class' => 'form-control', 'placeholder' => 'Barangay']) }}
        </div>
        <div class="form-group">
            {{ Form::file('profile_picture', ['class' => 'btn btn-primary']) }}
        </div>
    	{{ Form::hidden('_method', 'PUT') }}
    	{{ Form::submit('Submit', ['class' => 'btn btn-primary']) }}
	{!! Form::close() !!}
@endsection