{!! Form::open(["action" => ["UsersController@destroy", $users->id], "method" => "POST", "class" => "pull-right"]) !!}
    {{ Form::hidden("_method", "DELETE") }}
    {{ Form::submit("Delete", ["class" => "btn btn-danger"]) }}
{!! Form::close()!!}