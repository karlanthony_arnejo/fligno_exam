@extends('layouts.app')

@section('content')
	<a href="/users" class="btn btn-secondary">Go Back</a>
	<h1>{{ $user->first_name. " " .$user->last_name. "'s Profile." }}</h1>

	<div class="well">
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<img style="width:100%" src="/storage/profile_pictures/{{ $user->profile_picture }}">
			</div>
			<div class="col-md-4 col-sm-8">
				<h3>{{ $user->first_name. " " .$user->last_name }}</h3>
				<h4>{{ $user->description }}</h4>
				<h5>Gender: {{ $user->gender }}</h5>
				<h5>City: {{ $userAddress->city }}</h5>
				<h5>Barangay: {{ $userAddress->barangay }}</h5>
				<small>Created on: {{ $user->created_at }}</small>
			</div>
		</div>
	</div>
	<a href="/users/{{ $user->id }}/edit" class="btn btn-primary">Edit</a>
	
	{!! Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
		{{ Form::hidden('_method', 'DELETE') }}
		{{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
	{!! Form::close()!!}

<h3>Google Maps Address</h3>
    <div id="map"></div>
    <!--Diri ky kinopya ra ko ni sa net then change2 ra ang variables haha 2 ka snippets ako gi combine ani-->
    <script>
  		function initMap() 
  		{
		  var myLatLng = {lat: {{ $userAddress->lat }}, lng: {{ $userAddress->long }} };

		  var map = new google.maps.Map(document.getElementById('map'), {
		    zoom: 4,
		    center: myLatLng
		  });

		  var marker = new google.maps.Marker({
		    position: myLatLng,
		    map: map,
		    title: 'Hello World!'
		  });
		}
    </script>
    
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALncqTX7RbZ6Nwk7_Fdn1UyU3yY3zwbv4&callback=initMap">
    </script>
@endsection