<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	/*Follow naming convention. Laravel assumes your table name to be the plural form. Model name should be singular form.
	Or you can override your table name here. Example, model name: 'Customer'. DB Table name: 'customers'*/
	protected $table = 'customer';
}
