<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//after '@' is the function name from the PagesController
Route::get('/', 'PagesController@display_user');
Route::get('/contact', 'PagesController@contact');