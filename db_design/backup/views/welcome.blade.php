@extends('layout')

@section('content')

    <h1>My First Website</h1>

    <ul>
    <!-- TRADITIONAL PHP

        <?php /* foreach ($tasks as $task): */ ?>

            <li><? /* = $task; */ ?></li>

        <?php /* endforeach; */ ?>
    -->

    <!--Laravel blade-->
        @foreach ($mytask as $mytasks)

            <li>{{ $mytasks }}</li>

        @endforeach
    </ul>

@endsection

@section('title')

    Home

@endsection