<!DOCTYPE html>

<html>

<head>
    <title>@yield('title')</title>
</head>

<body>

	@yield('content')

	<ul>
	    <li><a href="/contact">Contact Us</a></li>
	    <li><a href="/">Home</a></li>
	</ul>

</body>

</html>