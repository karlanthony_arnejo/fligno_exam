<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function display_user()
    {
        //values to be passed
    	/*$mytask = 
		[
			"Go to the store",
			"Go to the market",
			"Go to work"
		];*/

        //passing the values to the view

        $mytask = User::fetch_all_users();

		return view('welcome', [

    		'mytask' => $mytask
    	]);
    }

    public function contact()
    {
    	return view('contact');
    }
}
