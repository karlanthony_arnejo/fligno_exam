<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public static function fetch_all_users()
    {
    	$data = User::all();

    	return $data;
    }

    public static function get_first_name()
    {
    	$data = User::all();

    	$first_name = $data->pluck('first_name');

    	return $first_name;
    }
}
