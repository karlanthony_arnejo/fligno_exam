<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
//profile
Route::get('/profile/{id}', 'PagesController@showProfile');
Route::get('/profile_edit/{id}', 'PagesController@edit');
Route::put('/profile_edit/{id}', 'PagesController@update');
Route::get('/services', 'PagesController@services');

#user resource
#routing order matters
//Route::get('/profile', 'UsersController@showProfile');
Route::get('users/yajraDT', 'UsersController@yajraDT');
Route::resource('users', 'UsersController');

#auth routes
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

#datatable sample
Route::get('create', 'DisplayDataController@create');
Route::get('index', 'DisplayDataController@index');